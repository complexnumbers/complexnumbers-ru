---
title: 'Утро нового тысячелетия — Complex Numbers'
description: Первый альбом Complex Numbers.
image: '/images/covers/mo402r.jpg'

album:
  title: Утро нового тысячелетия (1998–2003)
  description: Второй альбом Complex Numbers.
  artist: Complex Numbers
  cover: /images/covers/mo402r.jpg
  credits: |
    Музыка: Виктор Аргонов, Александр Асинский, Илья Пятов, Фрол Запольский. Текст: Виктор Аргонов. Вокал: Наталья Митрофанова.

  mp3: https://gitlab.com/api/v4/projects/32378434/packages/generic/release/1.0.0/2003-the-morning-of-the-new-millennium-mp3.zip
  srcBasic: https://gitlab.com/complexnumbers/morn-src/-/archive/master/morn-src-master.zip?path=src-basic
  vk: https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72225328
  bandcamp: https://complexnumbers.bandcamp.com/album/the-morning-of-the-new-millennium
  gdrive: https://drive.google.com/drive/folders/1nJXbIEGukk73qM8_WfpwTi03292Yrbeg
  youtube: https://www.youtube.com/playlist?list=OLAK5uy_lckNRdp1cHudQELbMReGCdPwEzn5RqhV4
  apple: https://music.apple.com/album/1140302510
  amazon: https://www.amazon.com/dp/B01JL167H8
  spotify: https://open.spotify.com/album/71SpmOpXYcA6dLKDGodEug
  yandex: https://music.yandex.ru/album/3658478

  tracks:
  - title: Эволюция
    description: Музыка — Илья Пятов
    url: https://complexnumbers.gitlab.io/releases/the-morning-of-the-new-millennium/mp3/01-the-evolution.mp3
  - title: Утро нового тысячелетия
    description: Музыка — Илья Пятов
    url: https://complexnumbers.gitlab.io/releases/the-morning-of-the-new-millennium/mp3/02-the-morning-of-the-new-millennium.mp3
  - title: Пробуждение [Версия 2003]
    description: Музыка, текст — Виктор Аргонов, вокал — Наталья Митрофанова
    url: https://complexnumbers.gitlab.io/releases/the-morning-of-the-new-millennium/mp3/03-awakening-2003-version.mp3
  - title: Экспансия
    description: Музыка — Фрол Запольский
    url: https://complexnumbers.gitlab.io/releases/the-morning-of-the-new-millennium/mp3/04-expansion.mp3
  - title: Солнечное затмение
    description: Музыка — Aster
    url: https://complexnumbers.gitlab.io/releases/the-morning-of-the-new-millennium/mp3/05-solar-eclipse.mp3
  - title: Цветущая земля
    description: Музыка — Виктор Аргонов
    url: https://complexnumbers.gitlab.io/releases/the-morning-of-the-new-millennium/mp3/06-earth-in-bloom.mp3
  - title: Время
    description: Музыка — Фрол Запольский, Aster
    url: https://complexnumbers.gitlab.io/releases/the-morning-of-the-new-millennium/mp3/07-time.mp3
  - title: Бесконечный путь
    description: Музыка — Виктор Аргонов
    url: https://complexnumbers.gitlab.io/releases/the-morning-of-the-new-millennium/mp3/08-engless-way.mp3
  - title: Трилогия [Версия 2000]
    description: Музыка — Aster
    url: https://complexnumbers.gitlab.io/releases/the-morning-of-the-new-millennium/mp3/09-triloly-2000-version.mp3
  - title: Неизбежность [Русская версия 2002]
    description: Музыка — Виктор Аргонов, вокал — Наталья Митрофанова
    url: https://complexnumbers.gitlab.io/releases/the-morning-of-the-new-millennium/mp3/10-ivevitability-2002-russian-version.mp3
  - title: Ещё не поздно начать всё сначала
    description: Музыка — Илья Пятов
    url: https://complexnumbers.gitlab.io/releases/the-morning-of-the-new-millennium/mp3/11-it-isnt-late-to-start-once-again.mp3
---
