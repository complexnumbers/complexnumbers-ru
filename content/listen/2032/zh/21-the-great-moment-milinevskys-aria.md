---
title: 伟大时刻
subtitle: 米利涅夫斯基的咏叹调
translation: https://www.bilibili.com/audio/au2036050
translator: Elizabeth
byArtist:
- name: Vitaliy Trofimenko
  role: 阿纳托利·米利涅夫斯基
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
<div class="poem">

**米利涅夫斯基**

历史的巨浪席卷着我们<br>
经过三十年风暴前的宁静<br>
我们仍然没有建立起<br>
我们希望的世界秩序。

我们受到了挑战，我不认为<br>
我们应该假装无事发生：<br>
如果大战已经无法避免，<br>
那就不必假传善意！

我看到了伟大时刻的来临<br>
多处地区性冲突的爆发<br>
可能最终<br>
将转化成世界革命！

就算这些想法都毫无道理<br>
战争也能给我们加速发展的机会<br>
所以，升起我们的旗帜<br>
如同红日一般，照耀整个世界！
</div>