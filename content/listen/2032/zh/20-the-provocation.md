---
title: 挑衅
translation: https://www.bilibili.com/audio/au2034941
translator: Elizabeth
byArtist:
- name: Ariel
  role: ASGU/全国自动化控制系统
  image: /images/photos/ari_1.jpg
- name: Vitaliy Trofimenko
  role: 阿纳托利·米利涅夫斯基
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---

**<u>[在米利涅夫斯基的房间]</u>**

**米利涅夫斯基：** 顺便问一下，你知道为什么我觉得你的声音很熟悉吗？

**ASGU：** 我的……声音？那是我！

**米利涅夫斯基：** 啊，没错…
那么，现在情况如何？

**ASGU：** 嗯……情况变得……更复杂了。
首先，伊朗军队不仅支持叛乱，而且入侵了阿富汗南部地区。
我们都知道那段边界的状况。
我试着朝他们开火，但是…没用。

**米利涅夫斯基：** 哇！…

**ASGU：** 还没完。
两个朝鲜哨站在凌晨3点被炸毁，凌晨5点他们在海上与敌人交火，目前还不清楚敌人的身份。
