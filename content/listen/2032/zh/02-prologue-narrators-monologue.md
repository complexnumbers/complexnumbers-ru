---
title: 序言
subtitle: 叙述者的独白
translation: https://www.bilibili.com/audio/au1822409
translator: Elizabeth
byArtist:
- name: Denis Shamrin
  role: 叙述者
  image: /images/photos/den_1.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---

<div class="poem">

**叙述者**

现在我们知道了，决定论无法解释世界。<br>
万物运行的规律比行星的运动更加复杂，难以理解…

我们只知道可能性，一切都被概率主导。<br>
在所有可能的情况中，只有一种会成真。

在大量潜在的道路中，我们只能选择一条。<br>
一旦当下成为历史，就不能被改变或消除。

但即使过去的一切悲剧<br>
&emsp;&emsp;&emsp;都不断地重演<br>
我们也无法预测未来<br>
&emsp;&emsp;&emsp;即便如此…

</div>
