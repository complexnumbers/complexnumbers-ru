---
title: 对不起！...
translation: https://www.bilibili.com/audio/au2031143
translator: Elizabeth
byArtist:
- name: Ariel
  role: 斯维特兰娜·利玛爱娃
  image: /images/photos/ari_1.jpg
- name: Denis Shamrin
  role: 代表
  image: /images/photos/den_1.jpg
- name: Vitaliy Trofimenko
  role: 阿纳托利·米利涅夫斯基
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---

**<u>[在执行委员会大礼堂，远景]</u>**

*&lt;掌声>*

**利玛爱娃：** 对不起！

**<u>[在执行委员会大礼堂，近景]</u>**

**米利涅夫斯基：** 哎呀……她晕倒了！

**代表：** 安纳托利·谢尔盖维奇！ 我认为这将成为丑闻。

**米利涅夫斯基：** 嗯，我不确定……但是为什么她会有这样的声音？

**代表：** 怎么了？

**米利涅夫斯基：** 这是ASGU的声音…

**代表：** 嗯，听起来很相似……很奇怪。

**米利涅夫斯基：** 告诉他们我想和她谈谈。
