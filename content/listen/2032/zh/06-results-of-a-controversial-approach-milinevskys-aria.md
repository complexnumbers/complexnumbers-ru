---
title: 智慧果
subtitle: 米利涅夫斯基的咏叹调
translation: https://www.bilibili.com/audio/au1831447
translator: Elizabeth
byArtist:
- name: Vitaliy Trofimenko
  role: 阿纳托利·米利涅夫斯基
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
<div class="poem">

**米利涅夫斯基**

&emsp;&emsp;&emsp;仅仅经过四年时间，<br>
一切都变得不同<br>
妳那水草丰茂的沃地<br>
&emsp;&emsp;&emsp;为我们结出了智慧之果！…

&emsp;&emsp;&emsp;一切对妳都很简单<br>
妳的眼光能穿透迷雾<br>
大地是如此完美无缺<br>
&emsp;&emsp;&emsp;不必担心时光流逝

&emsp;&emsp;&emsp;妳也从不会困扰于<br>
怀疑，犹豫和耻辱，<br>
妳也不在乎打破常规<br>
&emsp;&emsp;&emsp;不为任何决定后悔

&emsp;&emsp;&emsp;妳看起来知道如何解决<br>
困扰我们一生的难题<br>
可妳崛起的太快了！<br>
&emsp;&emsp;&emsp;我们真的能信任妳吗？

自动化呢？这是历史的进程<br>
&emsp;&emsp;&emsp;现在她已经席卷了全球！<br>
所以，曾经100年的斗争没有意义吗?<br>
&emsp;&emsp;&emsp;那一切革命，和伟大的胜利?

或许我们的目的是—机器共产主义，<br>
&emsp;&emsp;&emsp;可能劳动者将与过去完全不同？<br>
而集体主义不过是个返祖现象，<br>
&emsp;&emsp;&emsp;仅仅诞生于落后的生产力？！
</div>
