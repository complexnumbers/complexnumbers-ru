---
title: "2032：失落未来的传奇"
artist: Victor Argonov Project
playlistTitle: "2032<br>失落未来的传奇"
image: /listen/2032/cover-clean.jpg
overlay: /listen/2032/overlay-zh.svg

formatters:
  albumPageTitle: "%album_name%【%artist%】"
  trackPageTitle: "%track_title%【%album_name%】【%artist%】"
  trackTitle: "%track_title%【%track_subtitle%】"
  infoTitle: "%album_name%"

meta:
  description: 《2032：Legend of the Lost Future》是Victor Argonov Project创作的技术歌剧（techno-opera）。本剧讲的是1985年罗曼诺夫击败戈尔巴乔夫成为苏联总书记后，2032年的苏联和逐渐接管苏联的人工智能ASGU的故事。
---

**《2032：失落未来的传奇》** 是符拉迪沃斯托克的作曲家维克多·阿尔戈诺夫创作的第一部一小时半长的电子歌剧。

> 这既不是反乌托邦，也不是乌托邦或怪诞艺术。它更像是一次对共产主义思想，其病态的浪漫主义和悲剧，难以实现的基督教式博爱和理性主义经济目标的冲突的一次奇妙反思。个体之爱和普遍的爱，局部战争和世界大战，对地上天国的求索和自我毁灭的欲望，人造的理性和人类的理性…这不是支持或反对的宣传，而是提出了一个问题。就像在奇幻小说中，作者将故事设置在中世纪并不代表作者希望返回中世纪，在这里社会主义并不是政治性的，它只是创造了一种氛围，关于正在发生的事情的美感。

《2032：失落未来的传奇》被认为是第一部开源的电子歌剧，其歌曲基于[CC-BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)许可证于[https://complexnumbers.ru/2032/](https://complexnumbers.ru/2032/)。
