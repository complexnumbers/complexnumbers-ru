---
title: 程序员们
translation: https://www.bilibili.com/audio/au2032600
translator: Elizabeth
byArtist:
- name: Evgeniy Grivanov
  role: 第一个程序员
- name: Andrey Ivanov
  role: 程序员索科洛夫
- name: Vitaliy Trofimenko
  role: 阿纳托利·米利涅夫斯基
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---

**<u>[在控制大厅]</u>**

**米利涅夫斯基：** 她能听见我们说话吗？

**第一个程序员：** 不，现在不行。

**米利涅夫斯基：** 那么…？

**第一个程序员：** 呃…索科洛夫做了大部分工作

**程序员索科洛夫：** 她工作得很好。
尽管我们从未想过必须手动输入意识形态公理，但没有出现故障。
至少，你不在时没有。
她一切正常。

**米利涅夫斯基：** 嗯…
那很好。希望如此…
但我还有一个问题。
她为什么有那样的声音？

**程序员索科洛夫：** 什么样的声音？

**米利涅夫斯基：** 嗯，总的来说……就是这样。

**程序员索科洛夫：** 我认为是她自己建模的，她似乎喜欢这个。
当然，没人特意为她挑选声音…
