---
title: 进退两难
subtitle: 叙述者的独白
translation: https://www.bilibili.com/audio/au1831363
translator: Elizabeth
byArtist:
- name: Denis Shamrin
  role: 叙述者
  image: /images/photos/den_1.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
<div class="poem">

**叙述者**

我们坚信我们<br>
&emsp;&emsp;&emsp;通过自己的经验去发现真理。<br>
在理智的帮助下，<br>
&emsp;&emsp;&emsp;然后对结果进行总结。

当然，没有理由<br>
&emsp;&emsp;&emsp;在建造计算机时，给它增加意识形态的负担：<br>
它无论如何都会推导出<br>
&emsp;&emsp;&emsp;逻辑可演绎的一切，对吗？

但如果那台机器完美的大脑<br>
&emsp;&emsp;&emsp;拒绝了伟大的主义的<br>
一部分教条<br>
&emsp;&emsp;&emsp;甚至，质疑整个主义

那么，信仰坚定的人<br>
&emsp;&emsp;&emsp;绝不会畏惧那个数码之神，<br>
他们不太可能放弃自己的信仰：<br>
&emsp;&emsp;&emsp;宁愿认为是那台机器错了。

尽管如此…还是可以手动输入<br>
&emsp;&emsp;&emsp;所有的教条到那台机器的储存器里。<br>
这也并不困难。<br>
&emsp;&emsp;&emsp;然而，这台计算机也可能挣脱…
</div>