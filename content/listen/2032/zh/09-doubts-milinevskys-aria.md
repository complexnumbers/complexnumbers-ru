---
title: 疑问
subtitle: 米利涅夫斯基的咏叹调
translation: https://www.bilibili.com/audio/au1849152
translator: Elizabeth
byArtist:
- name: Vitaliy Trofimenko
  role: 阿纳托利·米利涅夫斯基
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
<div class="poem">

**米利涅夫斯基**

我知道我们是对的…我知道我是对的…<br>
&emsp;&emsp;&emsp;但仍有怀疑的阴影笼罩。<br>
今天，我们前所未有的强大<br>
&emsp;&emsp;&emsp;我们比以往任何时候都更强大。

真诚的兴奋着的人们<br>
&emsp;&emsp;&emsp;生活在对一个进步的社会结构的梦想中<br>
他们为自己的使命感到快乐<br>
&emsp;&emsp;&emsp;他们确实珍视自己的伟大梦想

时代似乎变了<br>
&emsp;&emsp;&emsp;高效涡轮机的轰鸣声已经远去<br>
但是，又一次，这强大的国家<br>
&emsp;&emsp;&emsp;正盲目地为自己的领袖欢呼

我们难道不是生活在宣传中<br>
&emsp;&emsp;&emsp;一旦事态失控，一切都会崩溃，<br>
当我们的进步仅仅<br>
&emsp;&emsp;&emsp;维系于机器运作的准确？！…
</div>