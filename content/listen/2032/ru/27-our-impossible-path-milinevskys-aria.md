---
title: Несбыточный свой путь
subtitle: ария Милиневского
byArtist:
- name: Виталий Трофименко
  role: Анатолий Милиневский
  image: /images/photos/vit_2.jpg
- name: Виктор Аргонов
  role: Музыка, либретто, звук, аранжировка
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Милиневский**

Что ждёт после жизни нас?<br>
&emsp;&emsp;&emsp;Лишь медленный сон без грёз,<br>
Лишь обморок, где вовек<br>
&emsp;&emsp;&emsp;Ни радости нет, ни слёз;<br>
И где за короткий срок<br>
&emsp;&emsp;&emsp;Пройдёт бесконечность лет,<br>
Которых, быть может, здесь,<br>
&emsp;&emsp;&emsp;В реальности, вовсе нет?

Что, если мы<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Погибнем от стремлений своих,<br>
&emsp;&emsp;&emsp;Погибнем от мечты,<br>
&emsp;&emsp;&emsp;Что, словно горизонт<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Уходит каждый миг?

В этой войне<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Найдём ли мы ответ в нас самих?<br>
&emsp;&emsp;&emsp;Несбыточный свой путь<br>
&emsp;&emsp;&emsp;Сумеем ли пройти<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;В условиях земных?

За что же мы ценим мир,<br>
&emsp;&emsp;&emsp;Что в чувствах даётся нам?<br>
Ведь он не всегда, отнюдь,<br>
&emsp;&emsp;&emsp;Подобен прекрасным снам.<br>
А может, она права:<br>
&emsp;&emsp;&emsp;Лишь в радости смысл всего,<br>
И, если наш мир - лишь боль,<br>
&emsp;&emsp;&emsp;Нам бросить пора его?<br>

Что, если мы<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Загадки ищем там. где их нет?<br>
&emsp;&emsp;&emsp;В вопросах, что есть жизнь<br>
&emsp;&emsp;&emsp;И в чём её цена<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Мне дан простой ответ.<br>
Но среди тьмы<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Найдём ли мы тот призрачный свет?<br>
&emsp;&emsp;&emsp;Несбыточный свой путь<br>
&emsp;&emsp;&emsp;Сумеем ли пройти<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;В дыму грядущих лет?

Как мне поверить в то,<br>
&emsp;&emsp;&emsp;Что я отныне тебя не увижу?<br>
Разве возможно так,<br>
&emsp;&emsp;&emsp;Что никогда я тебя не увижу?<br>
Вряд ли я мог тогда<br>
&emsp;&emsp;&emsp;Странное сходство воспринять серьёзно,<br>
Но полюбил, как сон,<br>
&emsp;&emsp;&emsp;Будто бы чудо и вправду возможно!

Тяжёлые капли дождя<br>
&emsp;&emsp;&emsp;Ложатся единой холодной стеною,<br>
И поезд бесшумно летит<br>
&emsp;&emsp;&emsp;Над чёрной магнитной стальной полосою,<br>
И в бешеной гонке огни<br>
&emsp;&emsp;&emsp;В ночь улетают, и больше неважно,<br>
Зачем этот мир вокруг нас,<br>
&emsp;&emsp;&emsp;Зачем появились мы в нём однажды.

Мир, где есть боль,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Утратил право жить так, как жил:<br>
&emsp;&emsp;&emsp;Он должен стать другим,<br>
&emsp;&emsp;&emsp;Иначе его жизнь<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Не стоит наших сил.<br>
Дождь над землёй<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Пыль прошлого из памяти смыл:<br>
&emsp;&emsp;&emsp;Несбыточный свой путь<br>
&emsp;&emsp;&emsp;Мы всё-таки пройдём.<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Пред нами мир застыл…
</div>
