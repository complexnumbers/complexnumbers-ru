---
title: Два месяца
subtitle: ария Рассказчицы
byArtist:
- name: Елена Яковец
  role: Рассказчица
  image: /images/photos/len_2.jpg
- name: Виктор Аргонов
  role: Музыка, либретто, звук, аранжировка
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Рассказчица**

Два месяца прошло.<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;В кольце кремлёвских стен<br>
&emsp;&emsp;&emsp;Затихли споры в коридорах власти,<br>
И до поры удержан<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Ветер перемен<br>
&emsp;&emsp;&emsp;Людьми, что в нём одном<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Увидели ненастье…

&emsp;&emsp;&emsp;На ней держалось всё,<br>
&emsp;&emsp;&emsp;Как, впрочем, и теперь,<br>
И тем сложнее было то решенье:<br>
&emsp;&emsp;&emsp;Все знали, что его<br>
&emsp;&emsp;&emsp;Нельзя осуществить,<br>
Чтобы не снизить эффективность управленья...<br>

К тринадцати годам<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Она сумела стать<br>
&emsp;&emsp;&emsp;На нас, порой, на удивление похожей:<br>
В неё влюбиться можно<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Было так легко,<br>
&emsp;&emsp;&emsp;И ревновать к ней власть<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Как к человеку, тоже…
</div>