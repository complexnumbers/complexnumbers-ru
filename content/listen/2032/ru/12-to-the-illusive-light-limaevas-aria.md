---
title: К призрачному свету
subtitle: ария Лимаевой
byArtist:
- name: Ariel
  role: Светлана Лимаева
  image: /images/photos/ari_1.jpg
- name: Виктор Аргонов
  role: Музыка, либретто, звук, аранжировка
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Лимаева**

Ночь<br>
&emsp;&emsp;&emsp;Гаснущего лета<br>
Россыпями звёзд горит<br>
&emsp;&emsp;&emsp;В сумерках рассвета.<br>
Я<br>
Вновь лечу сквозь память снов,<br>
В лабиринте грёз,<br>
&emsp;&emsp;&emsp;К призрачному свету!

Там<br>
Снова тот же день –<br>
День, который ваш принёс<br>
&emsp;&emsp;&emsp;Взгляд, и я узнала,<br>
Что<br>
В мире до него тогда<br>
Просто ничего<br>
&emsp;&emsp;&emsp;Не существовало.

Но, в этот миг, –<br>
&emsp;&emsp;&emsp;Только шаг между нами,<br>
И пол плывёт<br>
&emsp;&emsp;&emsp;У меня под ногами.

</div><br><div class="poem">

Пусть<br>
&emsp;&emsp;&emsp;Я не знаю, что должно случиться,<br>
Но<br>
&emsp;&emsp;&emsp;Я хочу, чтоб этот миг продлился,<br>
Сквозь<br>
&emsp;&emsp;&emsp;Бесконечность пролетев столетий,<br>
Лишь<br>
&emsp;&emsp;&emsp;Только видеть, что вы есть на свете.

</div><br><div class="poem">

Как<br>
&emsp;&emsp;&emsp;Я искала встречи –<br>
Встречи, о которой быть<br>
&emsp;&emsp;&emsp;Не могло и речи:<br>
Сквозь<br>
Толщу микросхем ваш взгляд<br>
Как звезда, далёк,<br>
&emsp;&emsp;&emsp;На Пути на Млечном.

Там<br>
Был со мной лишь сон –<br>
Сон, где мы так странно вдруг<br>
&emsp;&emsp;&emsp;Оказались вместе.<br>
Сон,<br>
Образ довершивший ваш,<br>
Лишь погас экран<br>
&emsp;&emsp;&emsp;В невесомом жесте.

Но в этот миг<br>
&emsp;&emsp;&emsp;Всё не так, всё иначе!<br>
Пусть, может быть,<br>
&emsp;&emsp;&emsp;Ничего я не значу,

</div><br><div class="poem">

Но<br>
&emsp;&emsp;&emsp;Этот мир живёт одним лишь вами,<br>
В снах<br>
&emsp;&emsp;&emsp;Озаряя небеса огнями,<br>
Я –<br>
&emsp;&emsp;&emsp;Лишь песчинка в бесконечности, но<br>
Дня<br>
&emsp;&emsp;&emsp;Нет без вас<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Для меня…

</div><br><div class="poem">

Всё!<br>
&emsp;&emsp;&emsp;Время безвозвратно –<br>
В вечность унесёт, и сон<br>
&emsp;&emsp;&emsp;Не вернуть обратно.<br>
Что<br>
Я могу поделать здесь?:<br>
Вы так далеко!<br>
&emsp;&emsp;&emsp;Время беспощадно!..
</div>
