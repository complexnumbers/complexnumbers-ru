---
title: Далёкий свет
subtitle: вокализ АСГУ / Лимаевой
image: /listen/2032/cover-clean.jpg
byArtist:
- name: Ariel
  role: АСГУ/Лимаева
  image: /images/photos/ari_1.jpg
- name: Виктор Аргонов
  role: Музыка, либретто, звук, аранжировка
  image: /images/photos/vic_1.jpg
---

