---
title: Programmers
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-16-%E2%80%93-%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%81%D1%82%D1%8B-2032-track-16-%E2%80%93-programmers.html
translator: FireyFlamy
byArtist:
- name: Evgeny Grivanov
  role: The 1st programmer
- name: Andrey Ivanov
  role: Programmer Sokolov
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

**<u>[At the control hall]</u>**

**Milinevsky:** Can she hear us talking?

**The 1st programmer:** No, not now.

**Milinevsky:** So..?

**The 1st programmer:** Well, comrade Sokolov did the most part…

**Programmer Sokolov:** She works just fine.
Even though, we’ve never thought we’d have to enter the ideologic axioms manually, but there was no malfunctioning.
At least, while you’ve been out.
She does everything as usual.

**Milinevsky:** Well…
That’s very good. I hope…
But I have one more question.
Why does she have a voice like that?

**Programmer Sokolov:** A voice like what?

**Milinevsky:** Well, in general… the way it is.

**Programmer Sokolov:** I think she modelled it herself, she seems to like this one.
Of course, nobody voiced her intentionally…
