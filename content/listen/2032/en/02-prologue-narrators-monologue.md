---
title: Prologue
subtitle: Narrator's monologue
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-02-%E2%80%93-%D0%BF%D1%80%D0%BE%D0%BB%D0%BE%D0%B3-2032-track-02-%E2%80%93-prologue.html
translator: FireyFlamy
byArtist:
- name: Denis Shamrin
  role: Narrator
  image: /images/photos/den_1.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

<div class="poem">

**Narrator**

So, now, we know that, in this world,<br>
&emsp;&emsp;&emsp;There is no place for fatalism:<br>
Its laws have turned out to be broader<br>
&emsp;&emsp;&emsp;And more difficult than the motions of planets…

We can only know the probability,<br>
&emsp;&emsp;&emsp;Everything is ruled by chance alone<br>
And, out of all the possible scenarios,<br>
&emsp;&emsp;&emsp;It gives us only one.

Out of numerous potential paths,<br>
&emsp;&emsp;&emsp;There's only one that becomes a reality,<br>
And, once it has become real,<br>
&emsp;&emsp;&emsp;It can be neither erased, nor crossed out;

But even if all the events of the past<br>
&emsp;&emsp;&emsp;Have been reproduced in the way they were,<br>
We couldn’t predict the plot of the future<br>
&emsp;&emsp;&emsp;Even then…

</div>
