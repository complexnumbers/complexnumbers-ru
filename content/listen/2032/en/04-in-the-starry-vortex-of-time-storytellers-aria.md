---
title: In the Starry Vortex of Time
subtitle: Narrator's aria
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-04-%E2%80%93-%D0%B2-%D0%B7%D0%B2%D1%91%D0%B7%D0%B4%D0%BD%D0%BE%D0%BC-%D0%B2%D0%B8%D1%85%D1%80%D0%B5-%D0%B2%D1%80%D0%B5%D0%BC%D1%91%D0%BD-2032-track-04-%E2%80%93-t.html
translator: FireyFlamy
byArtist:
- name: Elena Yakovets
  role: Narrator
  image: /images/photos/len_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Narrator**

In a starry vortex of time,<br>
&emsp;&emsp;&emsp;Dreams change their colors,<br>
Some of them have come true,<br>
&emsp;&emsp;&emsp;While others have completely vanished,<br>
But, there are the ones that have been attracting us with their strange light<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;For thousands of years,<br>
And yet they are so elusive, it’s just a dream<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;That still keeps its secrets.

But the more difficult our goal is to achieve,<br>
&emsp;&emsp;&emsp;The more alluring it is<br>
It makes us believe we can do it,<br>
&emsp;&emsp;&emsp;As if that goal was the only one that mattered…

Still believing in that dream,<br>
&emsp;&emsp;&emsp;Standing on the edge of a cliff,<br>
We’ve been searching for our own path,<br>
&emsp;&emsp;&emsp;Having forgotten about the endless lies<br>
That this terrestrial world<br>
&emsp;&emsp;&emsp;Was only for us, and only because<br>
We can destroy it<br>
&emsp;&emsp;&emsp;For the sake of the dream in just a moment.

And, maybe, it is so silly<br>
&emsp;&emsp;&emsp;To declare that our ultimate goal<br>
Is something like a naïve child's fairytale,<br>
&emsp;&emsp;&emsp;But it is worth living for!

In a starry vortex of time,<br>
&emsp;&emsp;&emsp;In terms of idea development,<br>
History is a strict judge,<br>
&emsp;&emsp;&emsp;But it’s the most reasonable one,<br>
The idea of the City of the Sun<br>
&emsp;&emsp;&emsp;Has been thought about for such a long time<br>
Yet it still hasn’t been realized,<br>
&emsp;&emsp;&emsp;We can only dream about it…

But the more difficult our goal is to achieve,<br>
&emsp;&emsp;&emsp;The harder it beckons<br>
It blinds us with its light<br>
&emsp;&emsp;&emsp;And we are embraced by this wonderful dream.

And, maybe, it is so silly<br>
&emsp;&emsp;&emsp;To declare that our ultimate goal<br>
Is something like a naïve child's fairytale,<br>
&emsp;&emsp;&emsp;But it is worth living for!
</div>
