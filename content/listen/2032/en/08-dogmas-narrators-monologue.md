---
title: The Dogma
subtitle: Narrator's monologue
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-08-%E2%80%93-%D0%B4%D0%BE%D0%B3%D0%BC%D1%8B-2032-track-08-%E2%80%93-dogmata.html
translator: FireyFlamy
byArtist:
- name: Denis Shamrin
  role: Narrator
  image: /images/photos/den_1.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Narrator**

We firmly believe that we always<br>
&emsp;&emsp;&emsp;Discover the truth through our own experience.<br>
It is understood with the help of reason,<br>
&emsp;&emsp;&emsp;And then the results are compiled.

And, of course, there’s no reason<br>
&emsp;&emsp;&emsp;To burden a computer with ideology while building it:<br>
It will calculate all the truths<br>
&emsp;&emsp;&emsp;That exist within reason anyway, right?

But if the machine’s perfect brain<br>
&emsp;&emsp;&emsp;Has rejected some parts<br>
Of the great doctrine, and,<br>
&emsp;&emsp;&emsp;Probably, questioned it as a whole,

Then, the one who firmly believes in those ideas<br>
&emsp;&emsp;&emsp;Won’t be afraid of that digital deity at all,<br>
They are unlikely to reject their beliefs:<br>
&emsp;&emsp;&emsp;It’s more likely that the machine is wrong.

Even though… It is still possible to write<br>
&emsp;&emsp;&emsp;All the dogma into the machine’s memory manually.<br>
And it’s not that difficult either,<br>
&emsp;&emsp;&emsp;However, there’s a chance that the computer might break...
</div>