---
title: 도발
translation: http://transural.egloos.com/314593
translator: GRU Fields
byArtist:
- name: 아리엘
  role: ASGU
  image: /images/photos/ari_1.jpg
- name: 빅토르 트로피멘코
  role: 아나톨리 밀리넵스키
  image: /images/photos/vit_2.jpg
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---

**밀리넵스키:** 그나저나, 왜 네 목소리가 그렇게 익숙한건지 넌 아는건가?

**ASGU:** 내... 목소리 말이야? 그건 나에요!

**밀리넵스키:** 아. 그래..
어찌 되가고 있나?

**ASGU:** 상황이라.. 말해주자면, 더 악화되고 있어.
첫번째로, 이란군은 반란을 도와주고 있는건 물론, 남아프간쪽으로 밀어 들어오고 있어.
그쪽 국경은 무슨일이 일어났는지 알아차렸을거야.
내가 그쪽으로 사격을 개시했지만, 그렇게 도움은 못됬어.

**밀리넵스키:** 이럴수가...

**ASGU:** 그게 다가 아니야,
3시쯤, 두 북한측 초소에서 폭발이 일어났고, 비무장 지대에서..
