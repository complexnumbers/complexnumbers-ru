---
title: 200분
translation: http://transural.egloos.com/314563
translator: GRU Fields
byArtist:
- name: 옐레나 야코베츠
  role: 여성 나레이터
  image: /images/photos/len_2.jpg
- name: 알렉산드르 아신스키
  role: 악기(기타) 부분
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**나레이터**

200분 거리...<br>
&emsp;&emsp;&emsp;천백킬로미터를 날아가며.<br>
해는 녹아만 가네.<br>
&emsp;&emsp;&emsp;불어오는 노을바람, 불꽃의 소용돌이 속에서.<br>
오늘에 남아 있던것도 이젠 끝.<br>
&emsp;&emsp;&emsp;거리가 멀어질수록, 하찮은 이야깃거리로 변하고.<br>
다른 문제들의 소음속에서<br>
&emsp;&emsp;&emsp;그저 평범하게 형식적인 호기심으로..

&emsp;&emsp;&emsp;우리는 그때까진 사랑이란건 불가능한 것인가요.<br>
지구속에서 우리는 떨어진채 모두의 사랑을 기다려야 하는건가요?<br>
&emsp;&emsp;&emsp;결국에 손을 맞잡을수는 없을려나..<br>
이 꿈같은 이야기 속에서..

우리는 그렇게는 못한답니다.<br>
&emsp;&emsp;&emsp;다른 사람의 감정을 들어주는 마음을<br>
무엇이 더욱 중요한지 느끼는 것을.<br>
&emsp;&emsp;&emsp;결국 남아있는건 - 통제 프로세스.<br>
자기보존(이기심)만 가지고 있죠.<br>
&emsp;&emsp;&emsp;인간의 삶보다 더 못하지는 않을거에요.<br>
프로세스의 오류는<br>
&emsp;&emsp;&emsp;일반적인 오류에서부터 우리에게 탈출구를 주죠.

&emsp;&emsp;&emsp;모순은 수많은 인간들에 있어.<br>
아름다운 삶을 살기에 오직 방해만 하는데.<br>
&emsp;&emsp;&emsp;아무런 문도 창도 없는 실체(모나드)의 왕국은<br>
그저 녹아버리는구나...

&emsp;&emsp;&emsp;오늘날의 비현실적인<br>
이세상 모두가 사랑하며 삶을 사는 꿈은<br>
&emsp;&emsp;&emsp;영원한 봄의 봉우리는 정복되지 않았네.<br>
이 꿈같은 이야기 속에서..
</div>