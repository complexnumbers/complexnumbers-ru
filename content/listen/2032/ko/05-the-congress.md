---
title: 당대회
translation: http://transural.egloos.com/314254
translator: GRU Fields
byArtist:
- name: 빅토르 트로피멘코
  role: 아나톨리 밀리넵스키
  image: /images/photos/vit_2.jpg
- name: 아리엘
  role: ASGU
  image: /images/photos/ari_1.jpg
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---

**ASGU:** ...소련 공산당 서기장이신 아나톨리 세르계예비치 밀리넵스키의 발언이 있겠습니다.

**&lt;당원들의 박수소리>**

**밀리넵스키:** 존경하는 (소비에트) 의원 여러분과 정치국원 여러분, 현재 2032년 7월 23일, 우리는 우리의 위대한 소련정부 영도자이자, 공산당 서기장 겸, 대학자 니콜라이 이오시포비치 플로트니코프를 기리며 당대회를 열게 되었습니다. 실질적인 안건들에 대해 언급 하기 전, 이자리를 빌어 저는 소련의 어떤 역사를 언급해 보고 싶습니다. 현재, 모두가 알다시피, 지나간 세기의 80년대 중반은 우리 조국은 심각한 경제상황이 발생했습니다..

**ASGU:**

때떄로 저 각각 당원들의 얼굴을 보는게 정말 흥미로워. :)<br>
만장일치, 안건에 대한 격론 없이.<br>
그들은 헛된 시간을 낭비하고 싶진 않아보이고, 나의 안건은 가볍게 통과할것으로 보이네,<br>
형식적 절차? 거의 필요없는 것.

**밀리넵스키:** ... 공산당 서기장 게오르기 바실레예비치 로마노프께서는 조국의 상황을 반전시키고, 세기가 변하는 시점에서 우리의 위치를 다시금 공고히 하는데에 노력했습니다. 서기장 니콜라이 플로트니코프께서는 우리가 가야할 위대한 방향을 제시했습니다. 기계 통제 이론, 인공지능 분야에서 놀라운 성과와,  대학자 플로트니코프의 직접적인 개발 참여로, 13년 전, ASGU(아스구), '국정자동운영체계'의 개발이 완료되었습니다. 이러한 체계의 개발은 20세기의 60년대의 키예프 키베르니치키(사이버네틱스) 연구소에서 연구소장 빅토르 글루쉬코프을 중심으로 진행되고 있었습니다. 계획경제의 효과 증진을 위한 것이였습니다. 4년전, 국가 정보망과의 융합으로 그녀(아스구)는 완전히 우리 정부의 체계와 수백만 소련 노동자 인민의 삶을 뒤바꿔 놓았습니다. 우리는 오늘도 그녀의 목소리로 보고를 받고 있고, 우리는 해외정책의 상황이 얼마나 바꿔졌는지도 알고 있습니다. 소련의 영토는 우리의 형제 국가 몽골과 북 아프가니스탄의 편입으로 인해 또다시 거대해졌고,  또다른 전환점은 지난 10년전, 중앙아시아 사회주의 연합체가 구성된 것도 있습니다. 지나간 구체제의 인민들은 최고의 체제를 선택한 겁니다...

<br>

**ASGU:** 존경하는 (소비에트) 의원 여러분과 정치국원 여러분. 현 안건에 따라, 국정자동운영체계의 보고가 있겠습니다. 저는 보고에 대해 여러분께 소개하고자..

**ASGU:**

*너, 벌써 여기 있는거야? 내 보고가 있을건데..?*

**밀리넵스키:**

알아, 근데 거기서 안듣는데 뭐가 문제인건가?<br>
나는 이미 너한테 말했잖아. 나는 완전히 동의는 못한다고.<br>
그건.. 너무 이르다는 걱정이 되는군..

**ASGU:**

...30년 뒤인 2062년 까지.... 당원들중의 일부는 이미 소위 "제 2 차 자동화 생산계획으로의 전환 테제"의 중요성을 깨닫고 있으며.. 중요 생산시설들의 통제권을 '저'에게로 전환하는 방안은, 우리들이 가지고 있는 반(半) 상품-화폐 체제의 완전한 탈피가 가능한 방안으로 인식되고 있습니다. 히지만, 보수파 쪽에서는 이에 대해 회의적인 시점을 가지고 있으며..


**밀리넵스키:**<br>
네 프로그램에는 오직 "기술"밖에 존재하지 않느냐?<br>
인간의 정신적 성장은 어디에도 없는데..?<br>
**ASGU:**<br>
이 시대에 계뭉주의 철학을 듣고 있는게 당황스러워.<br>
너도 알잖아. 노동 생산성이 어떤 사회든 가장 중요하다는 걸!<br>
**밀리넵스키:**<br>
차이는 있다. 노동하는 인간으로써 사는 것이던,<br>
그저 네 도움이나 받으며 손이나 깔짝거리는것은!

**ASGU:**<br>
난 노동을 하며 기뻐하는 사람들의 일을 빼앗지는 않아.<br>
**밀리넵스키:**<br>
그럼 얼마나 기생자들이 그 결과로 생겨날 것인가?<br>
**ASGU:**<br>
네 말이 난 아예 이해가 되질 않는걸?
