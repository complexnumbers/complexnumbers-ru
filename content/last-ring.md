---
title: 'Последнее кольцо — Complex Numbers'
description: Третий альбом Complex Numbers.
image: '/images/covers/la402r.jpg'
---

<div class="row">
<img src="/images/covers/la402r.jpg" class="albumcover-small" width="256px" />

<div class="albumdescription">

### Последнее кольцо (2007–2009)

Третий альбом Complex Numbers.

Музыка: Виктор Аргонов, Андрей Климковский, Александр Асинский, Hydrogen. Текст: Виктор Аргонов, Ariel. Гитарные партии: Александр Асинский. Вокал: Ariel, Наталья Митрофанова.

[Скачать mp3 и видео в zip](https://gitlab.com/api/v4/projects/32378444/packages/generic/release/1.0.0/2009-last-ring-mp3.zip)  [Скачать вокал и фонограммы](https://gitlab.com/complexnumbers/last-src/-/archive/master/last-src-master.zip?path=src-basic)

[Слушать главную песню альбома на Commons](https://commons.wikimedia.org/wiki/File:Complex-numbers--last-ring.opus)

[Слушать в VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72225350)

[YouTube](https://www.youtube.com/watch?v=XQ7uKEnKTxM)

[Jamendo](https://www.jamendo.com/album/488682/poslednee-kolco-izbrannoe)

[Audius на блокчейне](https://audius.co/complex_numbers/album/the-last-ring)

</div></div>

<div class="bandcamp-wrapper"><iframe style="border: 0; width: 100%; height: 504px;" src="https://bandcamp.com/EmbeddedPlayer/album=3547327098/size=large/bgcol=333333/linkcol=E49E11/artwork=none/transparent=true/" seamless>[Last Ring by Complex Numbers](https://complexnumbers.bandcamp.com/album/last-ring)</iframe></div>

## Треки и участники записи

1. [Последнее кольцо [Вокальная версия]](https://www.wikidata.org/wiki/Q105978624) Музыка - Андрей Климковский, Виктор Аргонов, текст - Виктор Аргонов, вокал - Ariel
2. **Ускорение [Версия 2007].** Музыка - Aster
3. **Цветные коробки.** Музыка, текст - Виктор Аргонов, вокал - Ariel
4. **KA-52 Аллигатор.** Музыка - Hydrogen, Виктор Аргонов
5. **Инновационные горизонты.** Музыка - Виктор Аргонов
6. **Трилогия. Часть 2 [Версия 2007].** Музыка - Aster
7. **Пути цивилизаций [Версия 2007].** Музыка - Фрол Запольский
8. **Inévitabilité [2009 french version].** Музыка - Виктор Аргонов, текст - Виктор Аргонов, Ariel, вокал - Ariel ([c английскими субтитрами](/swf/inevitabilite-en-fr/)/[swf](/swf/files/inevitabilite-en-fr.swf)))
9. **Неизбежность [Русская версия 2009].** Музыка, текст - Виктор Аргонов, вокал - Наталья Митрофанова ([c русскими субтитрами](/swf/inevitability-ru-ru/)/[swf](/swf/files/inevitability-ru-ru.swf), [c английскими субтитрами](/swf/inevitability-en-ru/)/[swf](/swf/files/inevitability-en-ru.swf))
10. **Последнее кольцо [Инструментальная версия].** Музыка - Андрей Климковский, Виктор Аргонов
