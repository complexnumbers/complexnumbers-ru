---
title: 'Techno symphony “Rethinking Progress”: synopsis'
---

<center>

**PART 1. CONSTRUCTING THE ENVIRONMENT**

</center>

<font color="8888FF">Since the most ancient times Man has been changing the world around him. However, until recently, the changes went slowly and were barely noticeable. It was during the Industrial Age when Mankind clearly realized the role of technological progress in its destiny.

It was an age of paradoxes unseen before. Fantastic power of Man — and his turning into a mere cog of the economic and political machine. The triumph of mass education — and the regress to truly medieval witchhunts. The victory over diseases that had plagued Mankind for millennia, the doubling and tripling of human life expectancy — and the most massive wars in history. Man's readiness to conquer and "humanize" other planets — and a very real chance of destroying his own.</font>


<div class="floatblock-wider">

![](/images/covers/p11e402.jpg)

</div>

<font color="00FF77">What are we? Where are we going? There was probably no other time in history when so many people were asking themselves these questions...</font>

<font color="8888FF">The Industrial Age is an illustration of the fantastic creative power of archaic and irrational human drives. Drives that were blind, sometimes outright destructive — yet still they changed human life for the better in the end of the day. The romance of industrialism was that of "progress for the sake of progress" with people being but means to a great end. Man was engineering a new world without thinking of himself. This period embraced the romance of power and speed, the romance of might and grand scales. Machines fulfilled the ancient dream of a magic force Man can master. This force inspired people to heroism and risk — even when the risk was taken with Mankind's own existence. Another deeply ingrained dream of Man made true by the industrial society was the dream of order. Despite the whirlwind pace of the changes, governments managed to make the lives of individuals delicately ordered and predictable. Universal employment, universal education, universal conscription. Standard dwelling, standard urban infrastructure, standard day schedule and lifestyle of citizens. No matter what values society declared — be they totalitarian or liberal. The logic of history demanded consolidating the efforts of great numbers of people around great projects.</font>

<font color="00FF77">Also... the Industrial Age taught us to fly. To fly faster than any wind, higher than any bird, to fly to any place on the Earth and even beyond its atmosphere.</font>

<font color="FF7700">Was all the bickering over political freedoms really so important during the epoch witnessing such dreams come true? Were all these mundane problems really so essential in the wake of the coming stellar destiny of Mankind?

There is no point in denying that the aerospace industry has always concerned itself primarily with military needs. It is obvious, though, that engineers' innermost motives reached far beyond that. Not about war and killing did they dream.

"How was it I did not see that lofty sky before? And how happy I am to have found it at last! Yes! All is vanity, all falsehood, except that infinite sky." Leo Tolstoy</font>

<div class="floatblock-wider">

![](/images/covers/p12e402.jpg)

</div>

<font color="8888FF">The period from the late 19th to the mid-20th century was unique as it saw the fastest progress in Mankind's history. The phenomenon of this upsurge is yet to be studied by historians, and it is not clear if something similar will happen again in the future. Maybe it was the very shock from such unprecedented progress and Mankind's failure to understand its real motives that ensured this pace. And just as people's minds sobered and they started to reflect over what was happening, progress began to slow down. By the 1970s, progress had satisfied the major needs of both common people and politicians, but started to betray the romantics' hopes one after another. Interplanetary flights, hypersonic aircraft, thermonuclear power plants, and a lot more remained but dreams. The industrial urban landscape took the form we know and has barely changed since, while some industries even saw a regress.</font>

<font color="FFFF00">Yes, the Industrial Age's dreams were irrational and naive. But they truly revolutionized people's worldview.</font>

<font color="FF7700">For the first time the general populace realized that the world could actually get better with time, rather than worse. And that progress can provide spiritual growth, not just mundane comfort.

The dream of the "Radiant Future" replaced the millennia-long grieving for a "Golden Age forever lost". Images of the coming Stellar Era outshined old children's tales and approached religious experiences in their significance.

It seemed sometimes that one could not even picture the future. It was like pure white light. Beautiful yet still outside our minds' grasp. And could another "Great Construction Project" or another giant machine bring people closer to this light? That was the question...</font>  

<center>

**PART 2. CONSTRUCTING INFORMATION**

</center>

<font color="8888FF">The second half of the 20th century saw information technology as a rare case of an industry continuing to develop while in most fields progress was slowing down. However, even here reality turned out to be different from what everyone expected.</font>

<div class="floatblock-wider">

![](/images/covers/p21e402.jpg)

</div>

<font color="00FF77">When the computer entered people's everyday lives in the 1980s, it did not do so as a "machine for calculations." It revealed to people a form of entertainment unseen before: video games. And with them — the notion of virtual reality, previously exclusive to science fiction.</font>

<font color="8888FF">During the Industrial Age people used to speak of "fairy tales coming true". It was a metaphor: technology gave Man access to "a magic force." But computers allowed him to literally draw a fairy­tale world on the screen and to enter this world. In the reality of a computer game, anyone could become a character of a fantasy novel, a spaceship pilot, or a brave knight. One could visit other planets, fight monsters, and even save humanity from death. And if the mission failed, one could always start over.</font>

<font color="00FF77">Primitive as the plots of the video games were, they still pulled the innermost strings of the human psyche. They often seemed more right and noble than everyday life. Players identified with game characters, saved their friends and loved ones together, and sometimes the very idea of an industrial paradise on Earth seemed to be a mistake.</font>

<font color="FF7700">Was it not better to stay forever in the fairy-tale world where good always triumphed over evil, and life always triumphed over death? Was it not the only true choice?</font>


<div class="floatblock-wider">

![](/images/covers/p22e402.jpg)

</div>

<font color="8888FF">As it often happened, life made its adjustments to these dreams. The romantic period of video gaming ended in the late 1990s. The increase in computers' technical specifications did not make games a "true" virtual reality. No longer a revelation, they became just another part of everyday life. However, by this time the fully featured personal computer had entered the mass consumers' homes, rather than just a gaming console. Digital equipment revealed its two new sides to the populace: production and communication. It became a universal means to create information products from software to literature, movies, music, and fine arts. Many creative activities that previously required specialized equipment cheapened by a large factor and became feasible at home. During the same years digital technology presented a fundamentally new paradigm of virtual reality to people: the Internet. Looking more like a newspaper than a new reality, it, nonetheless, had two important advantages over the already familiar games.

The first advantage was that users themselves could create this new virtual world. One could place texts, photos, videos, and music into the Internet, both new and digitized from old media.</font>

<font color="00FF77">Enthusiasts took full advantage of this, and, by the 2010s, almost the entire major scientific and creative legacy of Mankind was represented in the Internet. It became the World Library, "the Great Planetary Informatorium" the sci-fi authors dreamed of.</font>

<font color="8888FF">The second advantage of the Internet was the great number of its users. The Industrial Age allowed one to have hundreds of acquaintances, while now one could know tens of thousands of people. This communication explosion was akin to the very emergence of cities in Mankind's history. Just as cities destroyed traditional rural culture, so did the Internet cast doubt on conventional urban law, ethics, and employment relations. A completely new relationship system formed in the network — a communist anarchy of a kind.</font>

<font color="00FF77">"By electricity, we everywhere resume person-to-person relations as if on the smallest village scale" — Marshall McLuhan wrote. This was not a return to the archaic ways, though. Prohibition and coercion did not work here. People created content and spread knowledge for the sake of pleasure.</font>

<font color="FF7700">The problem was that the world was not limited to the Internet, and the "real life" was still too far from a utopia...</font>

<font color="FFFF00">Since the most ancient times people dreamed of utopias in other worlds: in heavens, on new continents, in outer space, in virtual reality. But the real changes always took place on Earth, in the real world, in the real society, in the psychology of real people. This held true for the Information Age as well.</font>

<font color="FF7700">Network anonymity and the access to hundreds of opinions on every issue taught people to understand their own interests better and become free from older ideals and taboos. Governments did not disappear, but they could no longer dictate morals and ideology to people. Wage labor did not disappear, but it was no longer an obligation.</font>

<font color="FFFF00">This transition was not easy, of course. Governments did not want to lose control over the minds of people and fought fiercely against the "lewdness" of the Internet. But censorship and repressions were powerless against the "hyperurbanization" trend, and banning the Internet completely was impossible as politics were secondary to the economy.</font>

<font color="00FF77">The diversity and the massive scale of network activities turned out to be beneficial to the business giants of the new era making profit off the network traffic. It was the number of network users, the amount of their free time, their right to visit any website and say and share what they liked that defined the economy. In the 2020s the new elites of leading countries "loosened the screws" and even introduced "basic income" — a welfare for all citizens regardless of their employment status. This reasserted the standing of hi-tech companies with high labor price, ruined the "work for food" companies, and legitimized the "free artists" class.</font>

<font color="FFFF00">The impact of these changes on people's worldview could be summed up with one phrase: Man ceased to be the "vessel of vice" in need of a stick and a carrot. People could finally see that, when left to themselves, they — just as Mankind as a whole — could actually get better with time, rather than worse.</font>  

<center>

**PART 3. CONSTRUCTING SENSATIONS**

</center>

<font color="DD00FF">In the 2030s, advances in biotechnology revived some fields of research which were previously only seen in the context of virtual reality. The spread of artificial organs — primarily, limbs and sensory organs — required creating a brain-computer interface.</font>

<div class="floatblock-wider">

![](/images/covers/p31e402.jpg)

</div>

<font color="00FF77">To transfer information between mind and machine accurately, one first needed to know the exact points where subjective sensations emerged in the brain. This technical problem spurred fundamental research and by the 2040s, a truly revolutionary discovery was made.</font>

<font color="00FF77">Scientists discovered the neural correlate of consciousness (NCC) — the brain's subsystem whose states unambiguously corresponded to states of consciousness. Simply put, the NCC was our "inner observer" — the very "soul" that philosophers had sought for many centuries.</font>

<font color="DD00FF">But now it was possible to measure and to engineer the states of this "soul" artificially. Manipulating one's state of consciousness through artificial means was possible before, but it was a kind of "alchemy" where the end result was hard to predict. But now the state of one's consciousness could be "custom built". One could, for example, make it identical to another being's state of consciousness. For the first time in history people could directly share any sensations (feelings, thoughts, etc.) — not only with each other but with other animals as well.</font>

<font color="00FF77">In the 19th century, even the materialist philosopher Friedrich Engels claimed that we would never know how ants see the ultraviolet color. But then came the 21st century. Aided by the new device called "state of consciousness interface (SCI)" humans could finally see this color, just like any other aspect of the animal inner world.</font>

<div class="floatblock-wider">

![](/images/covers/p32e402.jpg)

</div>

<font color="DD00FF">Meanwhile, research plunged deeper and deeper into the abyss of big philosophical questions. Structures analogous to the human NCC were found not only in animals but in plants and even non-biological objects as well. Data showed that even the cells in the human body could have their own primitive sensations, separate from our consciousness. They might even feel "good" or "bad" — they just could not tell the world about it.</font>

<font color="FF7700">It turned out that sensations permeated the entire Universe. It could be even said that the Universe consisted of sensations. The existence of each tiny grasshopper, each blade of grass, each grain of sand suddenly made sense within their own tiny worlds. The world turned out to be filled with meanings ineffable through mere formulas and numbers. But now they finally lay within science's grasp.</font>

<font color="DD00FF">In the meantime, new discoveries continued to shake the old worldview in its most sacred issues. While some researchers engaged in "telepathy", others studied the extreme states of consciousness with specific components "pumped up" to the maximum. With SCIs, volunteers could experience the absolute green color, the absolute salty taste, and — the maximum possible happiness or suffering. And this was where Mankind was lucky.</font>

<font color="DD00FF">The maximal "hell" was not just completely unbearable psychologically, but physically unstable as well. Cells either exited this state at once or just died.</font>

<font color="FF7700">But the maximal "heaven" was more stable and could last for minutes. People felt as if their entire lives were but a preparation for this ultimate experience. t was not a heroin-like "rush", it was a truly unique state. It combined all conceivable components of bliss in one. People who experienced it felt as though they merged with the Universe into a single entity, being completely consumed by love for all that exists. Essentially becoming this love.</font>

<font color="DD00FF">Without even expecting it, science found itself deep within the domain previously completely dominated by religions. It was this state of all­consuming love and complete enlightenment that religious hermits had been seeking for ages through meditation and asceticism. It was this "minute of world harmony" that Dostoevsky was experiencing before epileptic seizures, it was this "peak experience" that Maslow had got because of a stroke. It was this "plus four" state that Shulgin, Grof and other researchers of psychedelic and empathogenic drugs had sought so passionately.</font>

<font color="FF7700">Some theologians believed that a human being does not just "get to know" God through this state of absolute light, but that the state itself is God. Others yet claimed that the human being himself becomes God during the peak experience (the most radical interpretation of theosis). Absolute, all-merciful, all-forgiving, all-encompassing. Ineffable through words, impossible to grasp by those who have not had such an experience.</font>

<font color="FFFF00">Just as information technology did, neuroengineering brought about unexpected changes to Mankind. The purely technical problem of engineering a brain-computer interface and new psychotropic drugs resulted in yet another revolution in people's worldview. Of course, most scientists were skeptical about the religious interpretations of the new discoveries. But wasn't it just a matter of terminology? After all, even theologians had long since been trying to step away from simplified paternalistic views on God.</font>

<font color="FF7700">"We can no longer see God as an object, because we are speaking of unification, not just understanding." Vladimir Lossky.</font>

<font color="FF7700">"The object of apophatic theology is to move the question of truth and knowledge out of the domain of Greek ontology and into that of love and communion." John Zizioulas.</font>

<font color="FFFF00">And science did manage to reveal this love to Man. The only question remaining was how Man would manage this love.</font>  

<center>

**PART 4. CONSTRUCTING THE EVOLUTION**

</center>

<font color="DD00FF">Neither humans nor any other animals had ever been optimized through evolution to be happy. Using its "carrot and stick" approach, evolution forced living beings to fight for survival, without ever explaining why they should do it.</font>

<div class="floatblock-wider">

![](/images/covers/p41e402.jpg)

</div>

<font color="FF7700">Humans were the first animals to challenge this logic. They were the first to think about the meaning of life, and the first to dream of happiness as an abstract notion.</font>

<font color="DD00FF">By the mid-21st century, Man had achieved truly great results in adjusting nature to his needs. Industrial technology had rebuilt his habitat. It provided him with light, heat, and food in abundance. It protected him from the hazards of the wilderness. Information technology had changed human society. It eradicated war and government terror, poverty and unfree labor. Biotechnology had started modifying the very human body. It put an end to many diseases and promised near-immortality in the future. But did it all make Man truly happy? He shielded himself from many troubles, but psychologically he was still just another animal. There was still some crucial point where this world of high energies and high speeds was unlike the "Radiant Future" from a child's dream. Just as before, Man hurried somewhere, tried to achieve something, but the light of true happiness was still beyond his reach at the end of the tunnel. The ancient animal reward system kept him trapped in Brickman's and Campbell's "hedonic treadmill".</font>

<font color="FF7700">But finally, the treadmill had been broken. The state of consciousness interfaces (SCIs) drew a bottom line under the hundreds of millions of years of mental evolution. Science obtained the keys to all facets of human consciousness — including things like love and meaning.</font>

<div class="floatblock-wider">

![](/images/covers/p42e402.jpg)

</div>

<font color="00FF77">Implantable SCIs became widely available in the 2050s. They could be used for both "telepathy" and the direct stimulation of desired sensations, but with some limitations. Thus, feelings that could provoke escapism (such as the peak experience) were limited in their duration, while those that could trigger aggression were limited in their strength. The SCIs could only produce a powerful and prolonged euphoria when it was accompanied by "socially beneficial" drives — such as friendliness, helpfulness, and readiness to work. Its typical effect resembled that of phenethylamine stimulants and empathogens, but without any negative impact on health or intellect.</font>

<font color="DD00FF">Despite the limitations, the spread of the SCIs caused unforeseen and dramatic consequences. People sought shocks rather than joy. The Internet was quickly filled up with recordings of various subjective experiences. Some of them came from real life, while some were scripted. And many of them were quite dark. The suffering of the diseased and the dying, the distress of criminals and their victims, the terror of animals chased and devoured — all of this lured in the users. Though the safeguards of the SCIs did not allow the users descending into these horrors to suffer personally, they still experienced a very strong feeling. This feeling could be termed "euphoric compassion". People sympathized reveling in their sympathy, cried reveling in their tears. Artists had known this feeling for millennia and had been trying to reveal it to their audiences. But most people had never thought about it and were deeply shaken. As people realized the abyss of suffering still existing in the world, a great love for every sentient being was born in them, and the wish to save them all. It was the great rapture and the last judgment in one. So, to avoid a breakdown, the SCI safeguards turned on the peak experience for mere seconds.</font>

<font color="FF7700">The distress of mind subsided. The light of absolute happiness and love permeated people like a sweeping wind, like a summer shower rain, like a tsunami wave. They ran outside, swore brotherhood to each other, and repented all evil they had done upon anyone. They loved and blessed their enemies. There was no more pain in their happiness — only light. All that they wanted was to reveal it to others. And it was no longer possible to stop this "wave of enlightenment".</font>

<font color="FFFF00">Thus another page in the history of life on Earth had ended. Some could say that Mankind had built itself the Kingdom Come, while others would argue that it had just gained "manual control" over its motivational system.</font>

<font color="FF7700">Regardless, the "umbilical cord" connecting Man to his evolutionary roots was "cut". People's happiness no longer depended on natural programs and instincts. It depended solely on their own choice.

It is true that this transformation was risky. Had it happened a hundred years ago, Mankind could have perished as it drowned in this unprecedented new freedom. But this time Man had enough wisdom to find a compromise between happiness, self-preservation, and further development. People continued to improve SCIs and their software. They learned to create calmer emotional states of fascinating depth, not just extreme ones. What were they like? Were they like the feelings of Aleksandr Grin's Assol as she saw the ship of her dreams appear? But now people could experience such love at any time.

On the outside, however, civilization did not change very much. Many old commodities and forms of entertainment were no longer needed, but major industries still were. Healthcare and power supply, communications and fundamental science — all of this was necessary for people to live on and to share their happiness with other beings. Mankind still remained but a tiny island in the sea of merciless nature. But that was only the beginning. And this first experience had already proven many things. Happiness on demand turned out to be possible without becoming like a rat from Olds' and Milner's experiments, and universal love could exist without the ill effects of Lem's altruisin.

"The day will come when, after harnessing space, the winds, the tides, and gravitation, we shall harness for God the energies of love." These words by Teilhard de Chardin turned out to be prophetic. Hardly did he imagine, though, that his dreams would come true so soon...</font>  

<center>

**PART 5. BEYOND THE LINE**

</center>

<center>

**EPISODE 1. PROLOGUE**

</center>

<div class="row">
<div class="col">

<font color="DD00FF">

Night, rainbow-colored snow,  
        And silence outside,  
Only your heart  
        Is burning hot.  

Your tears stream down  
        In a saintly rain —  
The light of this love  
        Will remain with you forever...

</font>

</div>

<div class="col">

<font color="FF7700">

All the roads  
       Are quiet  
In this world  
       Where you suddenly grew up  
Before all the others  
       Who run through the mist  
       Longing to be happy in this unhappy world...  

You did not seek  
       This love,  
You only wanted  
       To glimpse a fantasy,  
But it is hard  
To predict the effect  
       Of these crystals...

</font>

</div>
</div>

<center>

**EPISODE 2. DIALOGUE**

</center>

<div class="row">
<div class="col">

<font color="8888FF">

Open a new page  
       And choose the colors  
That would paint us  
A picture of the shining world  
               Of future ages.  
But no matter what you draw,  
       It will hardly show us  
How progress  
Would change our minds,  
               The flow of our feelings.

</font>

<font color="FF7700">

Beyond the line  
There is a heavenly light,  
       More wonderful than any human dream!  
Should we  
Greet it,  
       Or stay away from it in dreams and in reality?  
Beyond the line  
There is a heavenly light.  
       Should we "open the door" boldly,  
Like you did  
This night?  
       Only your tears know...

</font>

</div>
<div class="col">

<font color="8888FF">

We are so used to the shackles  
       Of our ancient, animal urges —  
Riding on the rails  
Of our instincts,  
               Never deciding anything.  
It would be reasonable  
       If we changed our ways,  
But there is no power more terrible  
Than the power over ourselves,  
               And our reason is quiet.  

</font>

<font color="FF7700">

Beyond the line  
There is a heavenly light:  
       It is easier to see it than you think!  
To break the chains  
Of our old programs,  
       To let everyone be happy with no pain, no toil.  
Don't judge us,  
Don't try to deny it:  
       The world's had enough hypocrites and slaves!  
Just tell us —  
How are we going to live now  
       With this secret?...

</font>

</div>

</div>


<div class="row">
<div class="photocol">

![](/images/covers/p51e402.jpg)

</div>

<div class="col">

<font color="FF7700">"For several moments, I would experience such joy as would be inconceivable in ordinary life. I would feel the most complete harmony in myself and in the whole world. This feeling was so strong and sweet that for a few seconds of such bliss I would give ten or more years of my life, even my whole life perhaps" - Fyodor Dostoevsky recalled. These "peak experiences" were triggered by epilepsy. Dostoevsky was skeptical of the notion that they could be simulated by drugs. But just a century later, Shulgin would do that. He would say: "If a technique were ever to be discovered which would consistently produce this experience in all human beings, it would signal the ultimate evolution."</font>

</div>
</div>

<div class="row">
<div class="col">

<font color="8888FF">

We will never stop  
       The march of new discoveries,  
Yes, sometimes the substitutes  
Work much better  
              Than any "natural" solutions.  
We float in the sky,  
       Send messages down wires,  
Our world has long since outgrown  
The naive myth  
              That "natural is better".  

</font>

<font color="DD00FF">

Rebuilding our brain —  
       No problem at all!  
Press a button —  
       And feel anything you want!  

Forgive everyone,  
Love everyone —  
Anyone could do that,  
If "the door" were to be opened.

</font>

</div>

<div class="col">

<font color="FF7700">

But  
Can the world  
Weather the flame  
       Of this pure cosmic love?  
Would anyone  
Want to go on living  
       Once they have touched it?  
Beyond the line  
There is a heavenly light:  
       Press a button and meet it now,  
And you will understand:  
Our world is standing  
       On the edge...  

God, give us a chance  
       To live on,  
Help us not to drown  
       In this infinite love:  
We still have a long way to go,  
       There are still a lot of stars to reach,  
       The entire Universe for us to explore!  

And yet outside  
       Trillions of souls wait —  
Barely conscious,  
       So sad and so short-lived...  
They can't achieve  
       Happiness on their own.  
       Who can save them, if not we?..

</font>

</div></div>


<div class="row">
<div class="photocol">

![](/images/covers/p52e402.jpg)

</div>
<div class="col">

<font color="FF7700">Man proudly believes that the development of civilization is guided by his immutable passions and morals, while science and technology are only a means for this development. But ancient primates had been constructing and developing tools long before the emergence of double-articulated language or Homo sapiens as a species. Our biology and psychology, our passions and morals are products of technological progress. Man was not the one to invent progress. It was progress that shaped Man according to its trends. And our ethical and spiritual ideals are no exception to this. Today, they continue to develop along with our ability to change the world and ourselves.</font>

</div>

</div>

<center>

**EPISODE 3. EPILOGUE**

</center>

<div class="row"><div class="col">

<font color="DD00FF">

Night, rainbow-colored snow,  
       And silence outside,  
And your heart is pulsing  
       With the sorrows of the world.  

       Forgive everyone,  
       Love everyone —  
This is your gift now,  
       And your burden to bear...  

</font>

<font color="FF7700">

And outside  
       The first snow is falling,  
Coating buildings  
       In a rainbow paint,  
And the rays  
       From the street lights  
       Stretch like living beings in the yellow night.

</font>

</div>

<div class="col">

<font color="FF7700">

Do you want  
       To stay in this fantasy  
Never returning  
       To a world of pain?  
The means to do that  
       Have not been invented yet,  
       But remember this wonderful dawn!  

The snow outside  
       Will turn white again,  
Your body  
       Will calm down.  
You are able to sleep now,  
       And then you may return  
       To your everyday life.  

Maybe this world  
       Will also live on,  
It will not drown  
       In this infinite love,  
And the cosmic trail  
Of Earth's life  
       Will go on...

</font>

</div>
</div>
