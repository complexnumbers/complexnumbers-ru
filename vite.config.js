import {sveltekit} from '@sveltejs/kit/vite';
import svg from '@poppanator/sveltekit-svg';

/** @type {import('vite').UserConfig} */
const config = {
	plugins: [
		sveltekit(),
		svg({
			svgoOptions: {
				multipass: true,
				plugins: [{
					name: 'preset-default',
					params: {
						overrides: {
							mergePaths: false,
							removeViewBox: false,
						},
					},
				}],
			},
		}),
	],
};

export default config;
