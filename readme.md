# Complex Numbers Website

Using SvelteKit!

## Prerequisites

* [Node.js ≥ 16.x](https://nodejs.org/en/download/)
* [pnpm](https://pnpm.io/installation#using-a-standalone-script)

## Developing

In order to start a development server:

```bash
pnpm install
pnpm run dev

# or start the server and open the app in a new browser tab
pnpm run dev -- --open
```

## Building

In order to build the static version of website (including PWA), run:

```bash
pnpm install
pnpm run build
```

> You can preview the built app with `pnpm run preview`

## Tips

* Register redirects in `static/_redirects` file to keep backwards compatibility
* Pages with names like `test.en.md` are available under paths like `/en/test/`
* Feel free to use html markup in markdown files
* [HTML to Markdown converter](https://www.browserling.com/tools/html-to-markdown)
