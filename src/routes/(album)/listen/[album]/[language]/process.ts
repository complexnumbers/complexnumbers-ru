import {readSync} from 'to-vfile';
import {unified} from 'unified';
import remarkParse from 'remark-parse';
import remark2rehype from 'remark-rehype';
import rehypeStringify from 'rehype-stringify';
import rehypeRaw from 'rehype-raw';
import frontmatter from 'remark-frontmatter';
import * as yaml from 'js-yaml';
import {error} from '@sveltejs/kit';
import type {IndexPageMetadata, TrackPageMetadata} from '$lib/AudioPlayer/types';
import type {Root as SubsrtRoot} from '$lib/unified/subast';
import rehypeMergeSubtitles, {type MdastWithSubtitles} from '$lib/unified/rehype-merge-subtitles';
import subsrtParse from '$lib/unified/subsrt-parse';
import subastGetTimings from '$lib/unified/subast-get-timings';
import rehypeEllipsis from '$lib/unified/rehype-ellipsis';

const subsrtParser = unified()
	.use(subsrtParse, {format: 'ass'});

const remarkParser = unified()
	.use(remarkParse) // Parse Markdown to mdast syntax tree
	.use(frontmatter, ['yaml']); // Split front matter block

const runner = unified()
	.use(rehypeMergeSubtitles)
	.use(rehypeEllipsis)
	.use(remark2rehype, {allowDangerousHtml: true}) // Mutate to rehype
	.use(rehypeRaw) // Deal with HTML in Markdown
	.use(rehypeStringify); // Stringify hast syntax tree to HTML

export const processTrack = (album: string, language: string, track: string) => {
	// Parse subtitles
	const subtitlesFilename = `content/listen/${album}/${language}/${track}.ass`;
	let subtitlesFile: ReturnType<typeof readSync> | undefined;
	try {
		subtitlesFile = readSync(subtitlesFilename, 'utf8');
	} catch (error_: unknown) {
		if ((error_ as {code: string}).code !== 'ENOENT') {
			// eslint-disable-next-line @typescript-eslint/no-throw-literal
			throw error(500, String(error_));
		}
	}

	let timings: Array<[number, number]> | undefined;
	let subtitles: SubsrtRoot | undefined;

	if (subtitlesFile) {
		subtitles = subsrtParser.parse(subtitlesFile.value) as SubsrtRoot;
		timings = subastGetTimings(subtitles) as Array<[number, number]>;
	}

	// Parse yaml from markdown file
	const lyricsFilename = `content/listen/${album}/${language}/${track}.md`;
	let lyricsFile: ReturnType<typeof readSync>;
	try {
		lyricsFile = readSync(lyricsFilename, 'utf8');
	} catch (error_: unknown) {
		if ((error_ as {code: string}).code === 'ENOENT') {
			// eslint-disable-next-line @typescript-eslint/no-throw-literal
			throw error(404);
		}

		// eslint-disable-next-line @typescript-eslint/no-throw-literal
		throw error(500, String(error_));
	}

	// Split markdown file to yaml and content parts
	const tree = remarkParser.parse(lyricsFile);

	if (tree.children.length === 0 || tree.children[0].type !== 'yaml') {
		throw new Error('invalid markdown file');
	}

	const metadata = yaml.load(tree.children[0].value) as TrackPageMetadata;
	tree.children.shift();

	(tree as MdastWithSubtitles).subtitles = subtitles;

	// Generate html from markdown
	const content: string = runner.stringify(runner.runSync(tree));

	return {metadata, content, timings};
};

export const processIndex = (album: string, language: string) => {
	// Parse yaml from markdown file
	const lyricsFilename = `content/listen/${album}/${language}/index.md`;
	let lyricsFile: ReturnType<typeof readSync>;
	try {
		lyricsFile = readSync(lyricsFilename, 'utf8');
	} catch (error_: unknown) {
		if ((error_ as {code: string}).code === 'ENOENT') {
			// eslint-disable-next-line @typescript-eslint/no-throw-literal
			throw error(404);
		}

		// eslint-disable-next-line @typescript-eslint/no-throw-literal
		throw error(500, String(error_));
	}

	// Split markdown file to yaml and content parts
	const tree = remarkParser.parse(lyricsFile);

	if (tree.children.length === 0 || tree.children[0].type !== 'yaml') {
		throw new Error('invalid markdown file');
	}

	const metadata = yaml.load(tree.children[0].value) as IndexPageMetadata;
	tree.children.shift();

	// Generate html from markdown
	const content: string = runner.stringify(runner.runSync(tree));

	return {metadata, content};
};
