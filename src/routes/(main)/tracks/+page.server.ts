
import {readSync} from 'to-vfile';
import * as yaml from 'js-yaml';
import type {PageServerLoad} from './$types';

type TrackData = {
	title: string;
	releaseDate: string;
	musicBy: string;
	lyricsBy: string;
	performedBy: string;
};

export type AlbumData = {
	title: string;
	releaseDate: string;
	tracks: TrackData[];
};

type AlbumsData = {
	albums: AlbumData[];
};

export const load: PageServerLoad = () => {
	const file = readSync('content/tracks.yaml', 'utf8');
	return yaml.load(file.value.toString()) as AlbumsData;
};
