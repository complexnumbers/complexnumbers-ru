/// <reference types="@sveltejs/kit" />
/// <reference lib="webworker" />
import {build, files, version} from '$service-worker';

// Create a unique cache name for this deployment
const cacheName = `cache-${version}`;

const assets = [
	...build, // The app itself
	...files, // Everything in `static`
];

self.addEventListener('install', event => {
	// Create a new cache and add all files to it
	async function addFilesToCache() {
		const cache = await caches.open(cacheName);
		await cache.addAll(assets);
	}

	(event as ExtendableEvent).waitUntil(addFilesToCache());
});

self.addEventListener('activate', event => {
	// Remove previous cached data from disk
	async function deleteOldCaches() {
		for (const key of await caches.keys()) {
			if (key !== cacheName) {
				// eslint-disable-next-line no-await-in-loop
				await caches.delete(key);
			}
		}
	}

	(event as ExtendableEvent).waitUntil(deleteOldCaches());
});

self.addEventListener('fetch', (ev: Event) => {
	const event = ev as FetchEvent;

	// ignore POST requests etc
	if (event.request.method !== 'GET') {
		return;
	}

	// ignore cross-domain requests (ublock origin in Firefox breaks loading audio files from gitlab.io)
	if (event.request.mode === 'cors') {
		return;
	}

	async function respond(): Promise<Response> {
		const url = new URL(event.request.url);
		const cache = await caches.open(cacheName);

		// `build`/`files` can always be served from the cache
		if (assets.includes(url.pathname)) {
			const response = await cache.match(url.pathname);
			return response!;
		}

		// For everything else, try the network first, but
		// fall back to the cache if we're offline
		try {
			const response = await fetch(event.request);

			if (response.status === 200) {
				void cache.put(event.request, response.clone());
			}

			return response;
		} catch {
			const response = await cache.match(event.request);
			return response!;
		}
	}

	event.respondWith(respond());
});
