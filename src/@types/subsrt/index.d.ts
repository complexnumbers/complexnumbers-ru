declare module 'subsrt' {
	type SupportedFormats = 'sub' | 'srt' | 'sbv' | 'vtt' | 'lrc' | 'smi' | 'ssa' | 'ass' | 'json';

	type SimpleFormat = {
		build: any;
		detect: any;
		name: string;
		parse: any;
	};

	type TimeHelperFormat = SimpleFormat & {
		helper: {
			toMilliseconds: any;
			toTimeString: any;
		};
	};

	type HtmlHelperFormat = SimpleFormat & {
		helper: {
			htmlDecode: any;
			htmlEncode: any;
		};
	};

	export const format: {
		ass: TimeHelperFormat;
		json: SimpleFormat;
		lrc: TimeHelperFormat;
		sbv: TimeHelperFormat;
		smi: HtmlHelperFormat;
		srt: TimeHelperFormat;
		ssa: TimeHelperFormat;
		sub: SimpleFormat;
		vtt: TimeHelperFormat;
	};

	export type ParseOptions = {
		format?: SupportedFormats;
		verbose?: boolean;
		eol?: string;
		fps?: number;
		preserveSpaces?: boolean;
	};

	type CaptionLine = {
		type: string;
		index: number;
		start: number;
		end: number;
		duration: number;
		content: string;
		text: string;
	};

	type Captions = CaptionLine[];

	export type ResyncOptions = {
		offset?: number;
		ratio?: number;
		frame?: boolean;
	};

	export type BuildOptions = {
		format: SupportedFormats;
		verbose?: boolean;
		fps?: number;
		closeTags?: boolean;
	};

	export type ConvertOptions = {
		format: SupportedFormats;
		verbose?: boolean;
		eol?: string;
		fps?: number;
		resync?: ResyncOptions;
	};

	type Subsrt = {
		build(captions: Captions, options?: BuildOptions | undefined): string;
		convert(content: string, options?: ConvertOptions | string | undefined): string;
		detect(content: string): SupportedFormats;
		list(): SupportedFormats[];
		parse(content: string, options?: ParseOptions | undefined): Captions;
		resync(captions: Captions, options?: ResyncOptions | undefined): Captions;
	};

	const subsrt: Subsrt;

	export default subsrt;
}
