type CustomElement = Element & {
	previousY: number;
	previousRatio: number;
};

const trackSections = (element: HTMLElement) => {
	const thresholdArray = (steps: number) =>
		Array.from({length: steps + 1}, (_, index) => index / steps || 0);

	const sections = element.querySelectorAll('section');

	const observer = new IntersectionObserver(entries => {
		for (const entry of entries) {
			const currentY = entry.boundingClientRect.y;
			const currentRatio = entry.intersectionRatio;
			const isIntersecting = entry.isIntersecting;

			const target = entry.target as CustomElement;

			const previousY = target.previousY || 0;
			const previousRatio = target.previousRatio || 0;

			if (currentY < previousY) { // Scroll down
				if (currentRatio > previousRatio && isIntersecting) {
					entry.target.classList.add('scrolled');
				}
			} else if (isIntersecting) {
				entry.target.classList.add('scrolled');
			} else {
				entry.target.classList.remove('scrolled');
			}

			target.previousY = currentY;
			target.previousRatio = currentRatio;
		}
	}, {threshold: thresholdArray(20)});

	for (const section of Array.from(sections)) {
		section.classList.add('section-scroll');
		observer.observe(section);
	}

	// Console.log('Installed observers', sections);

	return {
		destroy() {
			for (const section of Array.from(sections)) {
				observer.unobserve(section);
			}
		},
	};
};

export default trackSections;
