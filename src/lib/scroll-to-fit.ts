const elementInterval = (element: HTMLElement) => {
	const top = element.offsetTop;
	const height = element.offsetHeight;
	return {top, bottom: top + height};
};

const elementIntervalMulti = (elements: HTMLElement[]) => {
	let top = Number.POSITIVE_INFINITY;
	let bottom = Number.NEGATIVE_INFINITY;
	for (const element of elements) {
		const ivl = elementInterval(element);
		top = Math.min(top, ivl.top);
		bottom = Math.max(bottom, ivl.bottom);
	}

	return {top, bottom};
};

const containerInterval = (element: HTMLElement) => {
	const top = element.scrollTop;
	const bottom = top + element.offsetHeight;

	return {top, bottom};
};

type Options = {
	elements: Array<string | Element>;
	container: HTMLElement;
	extraOffset?: number;
};

const scrollToFit = ({elements, container, extraOffset}: Options) => {
	extraOffset = extraOffset ?? 0;

	for (let i = 0; i < elements.length; i++) {
		const element = elements[i];
		if (typeof element === 'string') {
			elements[i] = document.querySelector(element);
		}
	}

	const containerIvl = containerInterval(container);
	const elementIvl = elementIntervalMulti(elements as HTMLElement[]);
	elementIvl.top -= extraOffset;
	elementIvl.bottom += extraOffset;

	if (elementIvl.top < containerIvl.top) {
		container.scrollTo({top: elementIvl.top, behavior: 'smooth'});
	} else if (elementIvl.bottom > containerIvl.bottom) {
		container.scrollTo({top: elementIvl.bottom - (containerIvl.bottom - containerIvl.top)});
	}
};

export default scrollToFit;
