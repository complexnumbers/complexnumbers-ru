import type {Plugin} from 'unified';
import {visit} from 'unist-util-visit';
import type {Root, Element} from 'hast';

export const rehypeAddLinksClasses: Plugin = () => (tree: Root) => {
	visit(tree, 'element', (node: Element) => {
		if (
			node.tagName === 'a'
            && node.properties
            && typeof node.properties.href === 'string'
		) {
			let className = 'link-generic';
			for (const child of node.children) {
				if (child.type === 'element' && child.tagName === 'img') {
					className = 'link-image';
				}
			}

			const currentClass = node.properties.class as string;
			node.properties.class = typeof currentClass === 'string' ? currentClass + ' ' + className : className;
		}
	});
};
