export type Track = {
	title: string;
	description: string;
	url: string;
	links?: Array<{
		text: string;
		url: string;
	}>;
};

export type AlbumMetadata = {
	title: string;
	artist: string;
	description: string;
	cover: string;
	credits: string;
	tracks: Track[];

	mp3?: string;
	srcBasic?: string;
	gdrive?: string;

	vk?: string;
	bandcamp?: string;
	apple?: string;
	youtube?: string;
	spotify?: string;
	yandex?: string;
};
